<?php
require_once (dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "hm_14" . DIRECTORY_SEPARATOR . "config.php");

$directory = ROOT_PATH . DIRECTORY_SEPARATOR;

$products = openJsonToArray($directory . "products.json");
?>

<!DOCTYPE html>
<html>
<head>
<style>
h1 {
    text-align: center;
    font-size: 50px;
}

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  margin-bottom: 30px;
}

td, th, p {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h1>Basket</h1>

<?php
    if (! empty($_GET['row'])) {
        $_SESSION["rows"] = $_GET['row'];
        arrayToTableChoosenRows($products, $_GET['row']);
    } else {
        unset($_SESSION['rows']);
        ?> <p> Your basket is empty </p> <?php
    }
?>

<form action="index.php" method="get">
  <input type="submit" name="Button" value="Back">
  <label for="Button"> To shopping </label>
</form>

</body>
</html>