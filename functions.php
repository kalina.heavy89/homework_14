<?php

//Function to creating json-file from txt-file if json-file isn't exists
function createFileJson ($addrJson, $addrTxt, $arrayKeys){
    if (! file_exists($addrJson)) {
        //Reading text file
        $fileString = file_get_contents($addrTxt);
        //Replacing symbols \t\n\r\0\x0B to space 
        $replSymbols = ["\t", "\r", "\n", "\0", "\x0B"];
        $fileString = str_replace($replSymbols, " ", $fileString); 
        //Deleting repeated spaces
        $fileString = preg_replace('/^ +| +$|( ) +/m', '$1', $fileString);
        //echo $fileString . "<br>";
        //Exploding string into array
        $fileArray = explode(" ", $fileString);
        //Creating array
        $sizeArrayKeys = count($arrayKeys);
        $i = 0;
        $j = 0;
        do {
            $array[$arrayKeys[$j++]][] = $fileArray[$i++];
            if ($j == $sizeArrayKeys) {
                $j = 0;
            }
            if (! isset($fileArray[$i])) {
                $fileArray[$i] = null;
            }
        }
        while ($fileArray[$i] == true);

        file_put_contents($addrJson , json_encode($array));
    }
}

//Function to opening json-file and creating array from it
function openJsonToArray($addrJson) {
    $fileJson = file_get_contents($addrJson);
    $array = json_decode($fileJson, true);
    return $array;
}

//Function to creating table from array with checkboxes in firsts coloumns
function arrayToCheckboxTable($arr){
    $arrLength = count($arr);
    $arrKeys = array_keys($arr);
    //Defining max length of subarrays from array $arr
    for ($i = 0; $i < $arrLength; $i++) {
        $subArrLength[$i] = count($arr[$arrKeys[$i]]);
    }
    $maxLengthSubArr = max($subArrLength);

    ?> <form action= "basket.php" method="get"> <?php
    //Showing higher row of table
    echo "<table>";
    echo "<tr>";
    for ($i = 0; $i < $arrLength; $i++) {
        echo "<th>" . $arrKeys[$i] . "</th>";
    }
    echo "</tr>";

    //Showing table elements from array $arr
    $i = 0;
    $k = 0;
    for ($i = 0; $i < $maxLengthSubArr; $i++) {
        echo "<tr>";
        for ($j = 0; $j < $arrLength; $j++) {
            isset ($arr[$arrKeys[$j]][$i]) ? $arr[$arrKeys[$j]][$i] : Null;
            if ($arr[$arrKeys[$j]][$i]) {
                echo "<td>";
                if($j == 0) {
                    ?>
                    <input type="checkbox"
                        id="row" 
                        name="row[]" 
                        value="<?php echo $i; ?>" 
                        <?php
                            if (! isset($_SESSION["rows"])) {
                                $_SESSION["rows"] = null;
                            } elseif (in_array($i, $_SESSION["rows"])) {
                                echo 'checked = "checked" ';
                            } ?>
                        /> <?php
                }
                echo $arr[$arrKeys[$j]][$i];
                echo "</td>";
            } else {
                echo "<td>" . " " . "</td>";
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    ?> <input type="submit" value="Order">
    </form> <?php
}

//Function to creating table from array with choosen rows
function arrayToTableChoosenRows($arr, $rows)
{
    $arrKeys = array_keys($arr);
    $arrLength = count($arrKeys);
    echo "<table>";
    echo "<tr>";
    for ($i = 0; $i < $arrLength; $i++) {
        echo "<th>" . $arrKeys[$i] . "</th>";
    }
    echo "</tr>";
    foreach ($rows as $row) {
        echo "<tr>";
            for ($i = 0; $i < $arrLength; $i++) {
                echo "<td>" . $arr[$arrKeys[$i]][$row] . "</td>";
            }
        echo "</tr>";
    }
    echo "</table>";
}