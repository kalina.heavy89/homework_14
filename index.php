<?php
require_once (dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "hm_14" . DIRECTORY_SEPARATOR . "config.php");

$directory = ROOT_PATH . DIRECTORY_SEPARATOR;

$arrayKeys = ["Name", "Price", "Quantity"];
createFileJson($directory . "products.json", $directory . "products.txt", $arrayKeys);

$products = openJsonToArray($directory . "products.json");
/*//Opening file products.json
$productsJson = file_get_contents($directory . "products.json");
$products = json_decode($productsJson, true);*/


?>

<!DOCTYPE html>
<html>
<head>
<style>
h1 {
    text-align: center;
    font-size: 50px;
}

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  margin-bottom: 30px;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h1>Food</h1>

  <?php arrayToCheckboxTable($products); ?>

</body>
</html>
